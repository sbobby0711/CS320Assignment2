#include <vector>
#include <stdexcept>
#include <string>
#include <sstream>
#include <cctype>
class Parser{
    public:
        bool Parse(std::vector<std::string> const &input);
};