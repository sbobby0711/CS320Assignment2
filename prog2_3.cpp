#include "prog2_3.hpp"

static bool validNumber(std::string s){
    if(s.size() == 0) return false;
    for(int i=0; i<s.size(); ++i){
        if(!isdigit(s[i])) return false;
    }
    return true;
}

bool Parser::Parse(std::vector<std::string> const &input){
    if(input.size() == 1){
        std::string s = input[0];
        return s == "pop" || s == "add" || s == "sub" 
           ||  s == "mul" || s == "div" || s == "mod"
           ||  s == "skip";
    }else if(input.size() == 2){
        std::string a = input[0], b = input[1];
        return  (a == "push" || a == "save" || a == "get")
             && validNumber(b);
    }
    return false;
}