#include <vector>
#include <stdexcept>
#include <string>
#include <sstream>
#include <cctype>
class Tokenizer{
    private:
        std::vector<std::string> tokens;
        bool valid(std::string s);
    public:
    Tokenizer(){
        
    }
    ~Tokenizer(){

    }
    void Tokenize(std::string str);
    std::vector<std::string> GetTokens();
};