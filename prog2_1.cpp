#include "prog2_1.hpp"

bool Tokenizer::valid(std::string s){
    if(s == "push") return true;
    if(s == "pop") return true;
    if(s == "add") return true;
    if(s == "sub") return true;
    if(s == "mul") return true;
    if(s == "div") return true;
    if(s == "mod") return true;
    if(s == "skip") return true;
    if(s == "save") return true;
    if(s == "get") return true;
    for(int i=0; i<s.size(); ++i){
        if(!isdigit(s[i])){
            return false;
        }
    }
    return true;
}
void Tokenizer::Tokenize(std::string str){
    std::stringstream ss(str);
    std::string s, err;
    while(ss >> s){
        if(valid(s)){
            tokens.push_back(s);
        }else{
            err = "Unexpected token: " + s;
            throw std::runtime_error(err.c_str());
        } 
    }
}
std::vector<std::string> Tokenizer::GetTokens(){
    std::vector<std::string> ret;
    if(tokens.size() == 0){
        throw std::runtime_error("No tokens");
    }else{
        ret.swap(tokens);
        return ret;
    }
}