#include "prog2_1.hpp"
#include <iostream>
#include <fstream>
#include <string>
int main(int argc, char **argv){
    using namespace std;
    if(argc < 2) {
        return -1;
    }

    cout << "Assignment #2-2, Weihao Chang, wchang930711@gmail.com" << endl;
    ifstream ifs(argv[1]);
    string line;
    int current = 0;
    Tokenizer tk;
    std::vector< std::vector<std::string > > outs;
    while(getline(ifs, line)){
        ++current;
        try{
            tk.Tokenize(line);
            outs.push_back(tk.GetTokens());
        }catch(std::exception &e){
            cout << "Error on line " << current << ": " << e.what() << endl;
            return -1;
        }
    }

    for(int i=0; i<outs.size(); ++i){
        for(int j=0; j<outs[i].size(); ++j){
            if(j > 0) cout << ",";
            cout << outs[i][j];
        }
        cout << endl;
    }
}